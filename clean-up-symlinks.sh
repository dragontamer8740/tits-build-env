#! /bin/sh
# should be runnable in POSIX shell.
# Please don't run this as root; I don't want something I didn't predict to
# cause out-of-tree link deletion.
# In fact, unless you're me, I'd recommend you don't use this at all.
if [ -z "$SRCDIR" ]; then
  SRCDIR="sourceTiTS"; export SRCDIR
fi
find -L "$SRCDIR"'/' -type l -delete
