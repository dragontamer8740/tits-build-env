#! /bin/bash
# builds a SWF for desktops.

# imagepack script sources this after setting XML_CONF_FILE.
# If it's not been set, then default to a non-imagepack build
if [ -z "$XML_CONF_FILE" ]; then
  XML_CONF_FILE='TiTSFDConfig.xml'; export XML_CONF_FILE
fi

# Source directory defaults to 'sourceTiTS' as dir name, because
# that's what the repo used to be called.
if [ -z "$SRCDIR" ]; then
  SRCDIR="sourceTiTS"; export SRCDIR
fi

# extract version number
# make sure grep behaves as we want, no customizations.
# Also don't complain if there is no grep alias
2>/dev/null unalias grep
# there is a literal tab character being matched in the following line, so
# be sure if you're using an IDE/"smart" editor that it's not changing it to
# spaces.
# This:
# 1) Gets the line where the version number is assigned in the source code
# 2) Removes all tab characters
# 3) Removes all spaces
# 4) Cuts everything from the start of the line to the characters '="'
# 5) Cuts everything after the closing double quote for the string.
VERSION="$(grep version.*=.* "$SRCDIR""/classes/TiTS.as" | sed 's/	//g;s/ //g;s/^.*="//;s/".*$//')"
export VERSION

if [ -z "$OUTPUT_SWF_FILE_BASENAME" ]; then
  OUTPUT_SWF_FILE_BASENAME='TiTS_'"$VERSION"; export OUTPUT_SWF_FILE_BASENAME
fi

if [ -z "$STACKMEM" ]; then
  STACKMEM='2048M'; export STACKMEM
fi

trimTrailingSlash()
{
  case "$1" in
    */)
      # has a trailing slash, remove it
      echo "$(echo "$1" | sed 's!/*$!!g' )"
      ;;
    *)
      echo "$1"
      ;;
  esac
}

# recreate GNU dirname in a shell function for portability.
# OS X, for example, does not have dirname (I think), since it uses mostly BSD
# utils and is only POSIX compliant at best. If anyone can implement this in a
# rigorously POSIX way, let me know.
# this takes precedence over the system's built-in dirname tool, if present.
dirname()
{
  INVAR="$(trimTrailingSlash "$1")"
  VAR="$(echo "$INVAR" |sed 's!/*$!!g'| sed 's!'"$(echo "$INVAR"|sed 's!^.*/!!g')"'!!g')"
  trimTrailingSlash "$VAR"
  # trimTrailingSlash will echo our output in the format expected for dirname
}
# find directory this script is located in. Works even through symlinks.
# stolen from stack exchange.
SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
  SCRIPTDIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
  SOURCE="$(readlink "$SOURCE")"
  [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE" # if $SOURCE was a relative symlink
done
SCRIPTDIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
#unset $SOURCE
unset SOURCE
cd "$SCRIPTDIR"
mkdir "$SCRIPTDIR""/bin" 2>/dev/null
cd "$SRCDIR"

rm -f "$SCRIPTDIR""/bin/""$OUTPUT_SWF_FILE_BASENAME"".swf"
rm -f "$SCRIPTDIR""/bin/""$OUTPUT_SWF_FILE_BASENAME""_uncompressed.swf"
# to port this next line to a bat script, replace 'set -o xtrace' with '@echo on'
set -o xtrace
java \
  "-Xmx""$STACKMEM" \
  -Xms512M \
  -Dsun.io.useCanonCaches=false \
  -Duser.language=en \
  -Duser.region=US \
  -Djava.util.Arrays.useLegacyMergeSort=true \
  -jar "$SCRIPTDIR""/FlashDevelop-plus-fl-libs-flex-AIR/Apps/flexairsdk/4.6.0+18.0.0/lib/mxmlc.jar" \
  +flexlib="$SCRIPTDIR""/FlashDevelop-plus-fl-libs-flex-AIR/Apps/flexairsdk/4.6.0+18.0.0/frameworks" \
  -load-config+="$SCRIPTDIR""/obj/""$XML_CONF_FILE" \
  -debug=true \
  -swf-version=22 \
  -o "$SCRIPTDIR""/bin/""$OUTPUT_SWF_FILE_BASENAME""_uncompressed.swf"
set +o xtrace
swfcombine -dz "$SCRIPTDIR""/bin/""$OUTPUT_SWF_FILE_BASENAME""_uncompressed.swf" -o "$SCRIPTDIR""/bin/""$OUTPUT_SWF_FILE_BASENAME"".swf"
if [ -e "$SCRIPTDIR""/bin/""$OUTPUT_SWF_FILE_BASENAME"".swf" ]; then # if swfcombine succeeded
  rm "$SCRIPTDIR""/bin/""$OUTPUT_SWF_FILE_BASENAME""_uncompressed.swf"
  chmod 644 "$SCRIPTDIR""/bin/""$OUTPUT_SWF_FILE_BASENAME"".swf"
else
  echo "==========================="
  echo "WARNING!!! - 'swfcombine' could not be run. Leaving an *uncompressed* SWF file"
  echo "in 'bin/' instead (assuming the build succeeded). This is sub-optimal. Please"
  echo "install 'swftools.'"
  echo "==========================="
fi
# o obj/TiTSFD636027137937680723
# '@echo off' in a windows bat.
#cd %OLDDIR%
#@echo on
if [ -e "$SCRIPTDIR""/bin/""$OUTPUT_SWF_FILE_BASENAME"".swf" ]; then
  echo "SWF output to 'bin/""$OUTPUT_SWF_FILE_BASENAME"".swf'."
elif [ -e "$SCRIPTDIR""/bin/""$OUTPUT_SWF_FILE_BASENAME""_uncompressed.swf" ]; then
  echo "SWF output to 'bin/""$OUTPUT_SWF_FILE_BASENAME""_uncompressed.swf'."
else
  echo "SWF may not have been created successfully. Please verify manually."
fi

