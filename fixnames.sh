#! /bin/bash

# this script is basically one huge ugly hack because the devs can't seem to
# figure out that the casing used in the file names should match what's in
# their code. Or maybe they just don't care about it.

# The first bit is mostly kept for legacy purposes, since at one point the
# imagepack files were a lot less organized (all in one huge dir together).
# It is only really definitely useful if building old versions of the source
# tree, but it won't hurt anything so it's left intact.
# There are still a few places in the old lines up top that are still
# necessary, however. Seriously, just leave it. It's magic. I know it's
# ugly, but it works(tm).

# There's also a brute-force linker that finds all dirs named 'hr' and
# makes links to them named 'HR', and also links all extant 'HR' dirs to 'hr'.
# It also creates cross-links so that both the 'hr' and 'HR' dirs contain all
# the same images (or at least links to them), since there is a possibility
# of having both 'hr' and 'HR' dirs present in the repo.
#
# This is warranted because I noticed an inordinate number of places where the
# casing problem wasn't in the filename itself but in the name of the
# directory. This fix makes it much, much more likely that a new update won't
# require manual intervention/changes in my script in order to fix.
#
# (Isn't windows such a great development platform?)
#

if [ -z "$OLDDIR" ]; then
  export OLDDIR="$PWD"
fi
if [ -z "$SRCDIR" ]; then
  export SRCDIR="sourceTiTS"
fi

cd "$SRCDIR""/includes/zhengShiStation"
ln -s forgeHound.as forgehound.as

cd "$OLDDIR"

cd "$SRCDIR""/assets/images/npcs/gats_old"
ln -s zilMale.png ZilMale.png
ln -s zilFemale.png ZilFemale.png

cd "$OLDDIR"

cd "$SRCDIR""/assets/images/npcs/bng"
ln -s "$PWD/HR/sig.png" "$PWD/hr/sig.png"

cd "$OLDDIR"

cd "$SRCDIR""/assets/images/npcs/gats"
ln -s celise.png Celise.png
ln -s shekka.png Shekka.png
ln -s v-ko_Nude.png V-Ko_Nude.png
ln -s v-ko.png V-Ko.png
ln -s penny.png Penny.png
ln -s sera.png Sera.png
ln -s kelly.png Kelly.png
ln -s jade.png Jade.png
ln -s flahne.png Flahne.png
ln -s syri.png Syri.png
ln -s kiro.png Kiro.png

cd "$OLDDIR"

cd "$SRCDIR""/assets/images/npcs/cheshire"
ln -s "$PWD/HR" "$PWD/hr"


cd "$OLDDIR"

cd "$SRCDIR""/assets/images/npcs/cyancapsule"
ln -s "$PWD/HR/sig.png" "$PWD/hr/sig.png"

cd "$OLDDIR"

cd "$SRCDIR""/assets/images/npcs/damnit"
ln -s "$PWD/HR/sig.png" "$PWD/hr/sig.png"

cd "$OLDDIR"

cd "$SRCDIR""/assets/images/npcs/docbadger"
ln -s "$PWD""/HR" "$PWD""/hr"

cd "$OLDDIR"

cd "$SRCDIR""/assets/images/npcs/shou_puppy"
ln -s warLion.png warlion.png
cd "$SRCDIR"

cd "$OLDDIR"
# in the wake of all of those above 'hr' vs 'HR' problems, I decided to
# automate some fixes.
hrfixer() {
  # high res fixes - might make the above obsolete.
  # cross-reference all images between 'hr/' and 'HR/' directories
  # so that all of them can be found in either location the code might specify.
  cd "$SRCDIR""/assets/images"
  find . -type d -name HR -print0 \
    | xargs -0 -I % sh -c 'set -x;
    cd %/../;
    if [ ! -e hr ]; then
      ln -s HR hr;
    else
      cd hr;
      for file in ../HR/*; do
        ln -s "$file" "$(basename "$file")";
      done;
      cd ../HR;
      for file in ../hr/*; do
        ln -s "$file" "$(basename "$file")";
      done;
    fi'

  find . -type d -name hr -print0 \
    | xargs -0 -I % sh -c 'set -x;
    cd %/../;
    if [ ! -e HR ]; then
      ln -s hr HR
    else
      cd HR;
      for file in ../hr/*; do
        ln -s "$file" "$(basename "$file")";
      done;
      cd ../hr;
      for file in ../HR/*; do
        ln -s "$file" "$(basename "$file")";
      done
    fi'

  cd "$OLDDIR"
}

echo "Running big bad brute force linker, first run"
2>/dev/null hrfixer > /dev/null

# more hr fixes for 'imagepack' builds (edit the XML configs)
cd "$SRCDIR""/assets/images/npcs/jay_echo/hr/"
ln -s Sellera.png sellera.png
ln -s Yarasta.png yarasta.png

cd "$OLDDIR"
cd "$SRCDIR""/assets/images/npcs/cheshire/hr/"
ln -s Sellesy.png sellesy.png
ln -s penny.png Penny.png
# some more stupid stuff because they can't even get the names close to right
# fisi instead of fisianna? Really? No QC at all I guess
ln -s fisianna.png fisi.png
ln -s fisianna_nude.png fisi_nude.png
ln -s fisianna_panties.png fisi_panties.png

cd "$OLDDIR"
cd "$SRCDIR""/assets/images/npcs/shou_puppy/hr/"
ln -s Ellie_Nude.png ellie_nude.png
ln -s Brynn_Nude.png brynn_nude.png
ln -s Brynn.png brynn.png
ln -s warLion.png warlion.png

# new imagepack fixes after official release of imagepacks (ugh)
cd "$OLDDIR"
cd "$SRCDIR""/assets/images/imagepack"
export IMAGEPACKDIR="$PWD"

# seer
cd seer
# lowercase and
ln -s Seer_And_Steele_by_Octomush.jpg Seer_and_Steele_by_Octomush.jpg

cd "$IMAGEPACKDIR"

# jade
cd jade
ln -s Rajii_Jade_BG_Nips.png rajii_Jade_BG_Nips.png
# jpg for some reason?
ln -s Rajii_Jade_BG.jpg rajii_Jade_BG.jpg
ln -s Tktktk_Jade_Shaded.png tktktk_Jade_Shaded.png

# shekka
cd "$IMAGEPACKDIR"
cd shekka
ln -s JamesAB_shekka_horsecock_fap.png jamesAB_shekka_horsecock_fap.png

# bianca Pee Enn Gee
cd "$IMAGEPACKDIR"
cd bianca
ln -s bianca_octomush_bentOver.PNG bianca_octomush_bentOver.png

echo "Running big bad brute force linker, second run"
2>/dev/null hrfixer > /dev/null
