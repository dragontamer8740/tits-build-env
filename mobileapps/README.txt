------FIRST OFF:------


I plan to write a script to help with this automagically at some point, but
haven't done so yet.

Requirements:

You will need:
  1) A Java JDK's jarsigner tool. Oracle's JDK works, as should OpenJDK.
  2) The info-zip programs (zip and unzip). Try `apt-get install zip unzip` in
     Debian and similar linux distros (e.g. Ubuntu).
     The info-zip homepage has downloads, too - see here:
     http://www.info-zip.org/
	3) A unix-like bourne style shell (korn shell should work, too, but I have
     not tested it yet).
  4) The Android SDK's "zipalign" tool - not strictly necessary, but improves
     memory usage and needs to be commented out of the scripts if not installed.

Operating System-Specific Requirements:

Linux/Unix:
  Wine is required for running some Windows programs packaged in the Adobe AIR
  SDK, unfortunately. This also means installing a windows version of the Java
  runtime (IIRC, a JDK is not strictly required) in Wine, in addition to the
  system's native Java runtime (AIR makes use of some java code as well, and
  when possible is executed in the normal host's version of Java).
  
  You will definitely need to edit the hardcoded paths in the tops of the .bat
  scripts according to the install locations of your Java JDK and Adobe AIR
  SDK. You can convert unix path names to windows/wine ones by running
  something like:
  `winepath -w '/home/user/.wine/drive_c/Program Files/Java/jre1.8.0_121/bin'`
  Protip: by default, Z:\ in wine is your home directory.

Windows:
  You will definitely need to edit the hardcoded paths in the tops of the .bat
  scripts according to the install locations of your Java JDK and Adobe AIR
  SDK.
  
  I have not tested this on windows. It should work in MSYS or Cygwin, but you
  will need to remove all references to Wine and have it execute the batch
  scripts properly. I _think_ based on my memories of using cmd that you would
  want to replace 'wine' with 'cmd /c' in MSYS. In cygwin, 'cygstart' would
  do it, I think.

OS X (technically a Unix, I guess, but weird):
  Funnily enough I have done zero testing in OS X. In the very distant past, I
  did create some AIR apps using OS X, but that was before I even had a 'build
  system' to speak of. It may be possible, and you might not even need wine
  if you opt to convert my simple .bat scripts back into POSIX shell scripts,
  since a version of the Adobe AIR SDK is officially supported on OS X.

  If the vendor were anyone but Apple, I'd probably have dug deeper into this
  by now. In any event, it should be possible, and quite probably less
  convoluted than the Wine deep magic I do.

Other notes:
The P12 (PKCS#12) certificate password is 1234. It expires on 26 May 2102.
It is not the cert I sign my builds with.

If trying to make iOS builds and all else fails, you can just unzip the IPA,
replace the SWF inside with your new one, and re-zip it. It won't be signed,
but you can still install it with AppSync just like you'd have to even if it
were signed.

It doesn't matter much for now, because we can't install our IPA's without
apple signing off on them anyway unless we force the phone to ignore the code
signatures.

My build system has been cleaned up a good deal for this release, so it is
possible I forgot to include something important. If I did, I am sorry.
Please let me know if I did forget anything. I'm really trying to make
building not absolutely insane for people.
