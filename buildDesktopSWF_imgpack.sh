#! /bin/bash
# builds a SWF for desktops, with the image pack functionality enabled.

# imagepack script sources normal buildDesktopSWF.sh script, after first
# setting XML_CONF_FILE to the name of our XML file.

# First, we make sure we have the right path to the script (if it's been
# called from out-of-tree).

trimTrailingSlash()
{
  case "$1" in
    */)
      # has a trailing slash, remove it                                                                                                                   
      echo "$(echo "$1" | sed 's!/*$!!g' )"
      ;;
    *)
      echo "$1"
      ;;
  esac
}

# recreate GNU dirname in a shell function for portability.
# OS X, for example, does not have dirname (I think), since it uses mostly BSD
# utils and is only POSIX compliant at best. If anyone can implement this in a
# rigorously POSIX way, let me know.
# this takes precedence over the system's built-in dirname tool, if present.
dirname()
{
  INVAR="$(trimTrailingSlash "$1")"
  VAR="$(echo "$INVAR" |sed 's!/*$!!g'| sed 's!'"$(echo "$INVAR"|sed 's!^.*/!!g')"'!!g')"
  trimTrailingSlash "$VAR"
  # trimTrailingSlash will echo our output in the format expected for dirname
}

# Source directory defaults to 'sourceTiTS' as dir name, because
# that's what the repo used to be called.

if [ -z "$SRCDIR" ]; then
  SRCDIR="sourceTiTS"; export SRCDIR
fi

# extract version number
# make sure grep behaves as we want, no customizations
# Also don't complain if there is no grep alias
2>/dev/null unalias grep
# there is a literal tab character being matched in the following line, so
# be sure if you're using an IDE/"smart" editor that it's not changing it to
# spaces.
# This:
# 1) Gets the line where the version number is assigned in the source code
# 2) Removes all tab characters
# 3) Removes all spaces
# 4) Cuts everything from the start of the line to the characters '="'
# 5) Cuts everything after the closing double quote for the string.
VERSION="$(grep version.*=.* "$SRCDIR""/classes/TiTS.as" | sed 's/	//g;s/ //g;s/^.*="//;s/".*$//')"
export VERSION

SCRIPTPATH="$(dirname "$0")"

# This takes precedence over the default when we call buildDesktopSWF.sh
XML_CONF_FILE="TiTSFDConfig_IMAGEPACK.xml"; export XML_CONF_FILE

OUTPUT_SWF_FILE_BASENAME='TiTS_imgpack_'"$VERSION"; export OUTPUT_SWF_FILE_BASENAME

STACKMEM="6144M"; export STACKMEM # 6 gigabytes (!)

# '.' == 'source' in bash-speak. the '.' is standardized, though, so it is
# therefore preferred.
. "$SCRIPTPATH""/buildDesktopSWF.sh"
